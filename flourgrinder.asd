(asdf:defsystem #:flourgrinder
    :description "A command-driven chat bot which features markov chains."
    :author "Jay Kamat <jaygkamat@gmail.com>"
    :license "GPLv3"
    :serial t
    :depends-on (:hunchentoot
                 :jonathan
                 :cl-who
                 :drakma
                 :cl-store
                 :unix-opts
                 :cl-ppcre
                 :str
                 :trivial-string-template
                 :alexandria
                 ;; Needs manual install :(
                 ;; https://github.com/lispcord/lispcord
                 :lispcord
                 :bordeaux-threads)
    :components ((:module "src"
                          :components
                          ((:file "package")
                           (:file "markov-text-generator")
                           (:file "weapons")
                           (:file "markov-driver"
                                  :depends-on ("markov-text-generator"))
                           (:file "throttle")
                           (:file "good-boy")
                           (:file "flourgrinder"
                                  :depends-on ("markov-driver" "throttle" "weapons" "good-boy"))))))
