(in-package :flourgrinder)

;; TODO make this path configurable
(defparameter *userhist-file* "./seeds/userhist")
(defun load-userhist ()
  (cl-store:restore *userhist-file*))
;; TODO handle failure here
(defparameter *userhist* (load-userhist))
(defparameter *search-size* 1000)
(defparameter *max-seed-size* 1000000)
(defun save-userhist ()
  (cl-store:store *userhist* *userhist-file*))
(defparameter *offset-count* 0)
(defparameter *offset-save* 20)

(defun user-text (user limit)
  "Get properly formatted text for USER."
  (let* ((user-list (gethash user *userhist*)))
    (when (and limit (< limit (length user-list)))
      (setf user-list (subseq user-list 0 limit)))
    (str:join (string #\NEWLINE) user-list)))

(defun user-regex-text (user-regexp limit)
  "Get properly formatted text for USER."
  (let ((result-lists
         (mapcar (a:rcurry #'user-text limit)
                 (delete-if-not (a:curry #'cl-ppcre:scan user-regexp)
                                (a:hash-table-keys *userhist*)))))
    (when result-lists
      (str:join (string #\NEWLINE) result-lists))))

(defun text-pick (text)
  "Pick and clean up text after generation for sending into groupme."
  (let* ((split-text (str:lines text :omit-nulls t))
         (sorted-split (sort split-text #'> :key #'length))
         (longest-match (first sorted-split))
         (longest-match (str:prune (limit) longest-match)))
    longest-match))

(defun user-markov-generation (user &key
                                      (markov-sanity)
                                      (regexp nil)
                                      (limit nil))
  (when (or (gethash user *userhist*) regexp)
    (a:when-let* ((markov-sanity (or markov-sanity 6))
                  ;; Clamp markov-sanity
                  (markov-sanity (a:clamp markov-sanity 1 10))
                  (user-text (if regexp
                                 (user-regex-text user limit)
                                 (user-text user limit)))
                  (user-text (if (> (length user-text) *max-seed-size*)
                                 (subseq user-text 0 *max-seed-size*)
                                 user-text))
                  (user-text (if (< (length user-text) markov-sanity)
                                 ;; When our user-text is too short, abort
                                 nil
                                 user-text))
                  (raw-text (generate-text
                             markov-sanity
                             user-text
                             *search-size*))
                  (longest-match (text-pick raw-text)))
      longest-match)))

(defun inline-markov-generation (text &key
                                        (markov-sanity)
                                        (regexp nil)
                                        (limit nil))
  (declare (ignore limit regexp))
  (a:when-let* ((markov-sanity (or markov-sanity 6))
                (raw-text (generate-text markov-sanity text *search-size*))
                (longest-match (text-pick raw-text)))
    longest-match))

(defun user-random-message (user)
(when (gethash user *userhist*)
  (a:random-elt (gethash user *userhist*))))

(defun user-add-hist (user text)
  (push text
        (gethash user *userhist*))

  (incf *offset-count*)
  (when (> *offset-count* *offset-save*)
    (save-userhist)
    (setf *offset-count* 0)))


(defun user-find-sources (match-regexp)
  "Get the earliest message which matches MATCH-REGEXP for each user."
  (let ((matches))
    (maphash
     (lambda (user text-list)
       (unless (equal *BOT-NAME* user)
         (let ((match-line
                (find-if
                 (a:curry #'cl-ppcre:scan match-regexp)
                 (reverse text-list))))
           (when match-line
             (push (cons user match-line) matches)))))
     *userhist*)
    matches))
