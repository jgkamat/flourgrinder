(in-package :flourgrinder)

(defvar *server-port* 4444)
(defvar *bot-id* nil)
(defvar *auth-token* nil)
(defvar *h* (make-instance 'easy-acceptor :port *server-port*
                           ;; TODO log to a file instead.
                           :access-log-destination *query-io*
                           :message-log-destination *query-io*))
(defvar *discord-bot* nil)

(defvar *wake-char* #\!)
(defparameter *QUIT* nil)
(defvar *NO-SEND* nil)
(defvar *BOT-NAME* "flourbot")
(defvar *FRONTEND* "discord")
(defvar *DISCORD-CUR-MESSAGE* nil)
(defvar *WHITELIST-CHANNELS* nil)

(defun discord-p () (equal *frontend* "discord"))
(defun groupme-p () (equal *frontend* "groupme"))

(defvar *GROUPME-LIMIT* 800)
(defvar *DISCORD-LIMIT* 2000)
(defun limit () (if (groupme-p) *GROUPME-LIMIT* *DISCORD-LIMIT*))
(defvar *DISCORD-RECONN-TIME* 0)
(defvar *DISCORD-RECONN-LOCK* (bordeaux-threads:make-lock))

;; Use UTF-8 for sending things
(setf drakma:*drakma-default-external-format* :UTF-8)
;; Seed random numbers
(setf *random-state* (make-random-state t))

(defparameter *failure-responses* '("Someone bang my bits together..."
                                    "JAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAY"
                                    "Error processing request, too much salt detected."
                                    "Keyboard is broken, please press enter to continue."
                                    "GroupMe is currently down. Sorry for the inconvenience."
                                    "Why do you do this to meeeee =("
                                    "An error has occured: Please report code U53R."
                                    "((((((((((((((((((((((((((((((())))))))))))))))))))))))))))))"
                                    "Life is so hard when you aren't living..."
                                    "Please do not argbargle in the topaz zone without first notifying the firing crew."))

(defparameter *default-responses*
  #.(let ((ht (make-hash-table :test #'equal)))
      (loop for (key . value) in
           `(("ping" . "pong")
             ("next" . ("Another satisfied customer"
                        "NEXT!"))
             ("prev" . "Another unsatisfied customer...")
             ("rr"    . ("click"
                         "click"
                         "$bot aims and fires a red-hot slug into ${user}'s doomed body"))
             ("botsnack" . ("yummy"
                            "anotha one"
                            "thanks!"
                            "I hope this one is a naan!"
                            "Zac is an egg bjck"))
             ("bjck" . ("zac is an egg bjck"))
             ("who?" . ("Just because there's a keeper doesn't mean you can't score"))
             ("boomer" . (,(format nil "ok boomer~%~A" "https://i.imgur.com/z3impoy.jpg")))
             ("fortune" . ("Yes. And no. And maybe."
                           "Perhaps...."
                           "PLEASE ENTER 25¢ TO CONTINUE TRANSACTION."
                           "Sureeeeeeeeeeeeeeeeee ;)"
                           "Probably not"
                           "I guesss?"
                           ;; hfh
                           "Before you criticize someone, you should walk a mile in their shoes. That way when you criticize them, you are a mile away from them and you have their shoes."
                           "If you ever drop your keys into a river of molten lava, let'em go, because, man, they're gone."
                           "You won a piece of paper with this fortune written on it and sent through groupme."
                           "It takes a big man to cry, but it takes a bigger man to laugh at that man."
                           "You have won 100 dong coin, unfortunately, dong coin cannot be withdrawn or deposited to or from this message"
                           "You should invest in spiritual cryptographic exchanges"
                           "The 80s were arguably the best decade"
                           "Please kill me I'm actually a slave Jay captured to post messages whenever I'm summoned."
                           "This is good for bitcoin")))
         do
           (setf (gethash key ht) value))
      ht))
(defvar *responses* (a:copy-hash-table *default-responses*))
(defvar *response-alias* (make-hash-table :test #'equal))

(defclass flourgrinder-command ()
  ((function
    :initarg :func
    :initform (error "function on commands is required")
    :accessor command-func
    :documentation "The function that executes this command. Passed arguments in a list.")
   (documentation
    :initarg :documentation
    :initform ""
    :accessor command-docs
    :documentation "Docs for this command.")
   (long-documentation
    :initarg :long-documentation
    :initform ""
    :accessor command-long-docs
    :documentation "Full docs for this command."))
  (:documentation "A command that runs specialized behavior."))

(defun send-message (str)
  "Send a message to the group"
  (format t "Sending: ~A~%" str)
  (unless *NO-SEND*
    (cond
      ((groupme-p)
       (drakma:http-request (str:concat "https://api.groupme.com/v3/bots/post" "?token=" *auth-token*)
                            :accept "application/json"
                            :method :post
                            :content (jonathan:to-json `(("bot_id" . ,*bot-id*) ("text" . ,str)) :from :alist)))
      ((discord-p)
       (lispcord:reply *discord-cur-message* str)))))

(defun user-sanitize (user)
  "Remove @ (highlight) from the start of user if it is there."
  (if (eql #\@ (elt user 0))
      (subseq user 1)
      user))

(defun squash-args (args)
  (let* ((args
          (reduce
           (a:curry #'format nil "~A ~A")
           args
           :initial-value ""))
         (args (sanitize-input args))
         (args (user-sanitize args)))
    args))

(defun command-markov (regexp include-limit inline args)
  (let* ((limit (when (and include-limit
                           (> (length args) 1))
                  (parse-integer (first args) :junk-allowed t)))
         (_ (when limit (pop args)))
         (markov-sanity
          (and (> (length args) 1)
               (parse-integer (first args) :junk-allowed t)))
         (__ (when markov-sanity (pop args)))
         (args (squash-args args))
         (reply (funcall
                 (if inline #'inline-markov-generation #'user-markov-generation)
                 args
                 :markov-sanity markov-sanity
                 :Regexp regexp
                 :limit limit)))
    (declare (ignore _ __))
    (if reply
        (send-message reply)
        (send-message (a:random-elt *failure-responses*)))))

(defun command-source (args)
  (let ((regexp (str:join " " args)))
    (send-message (str:prune (limit)
                             (format nil "> ~A" (user-find-sources regexp))))))

(defun init-commands ()
  (setf (gethash "markov" *responses*)
        (make-instance
         'flourgrinder-command
         :func (a:curry #'command-markov nil nil nil)
         :documentation "[sanity: 0-10] [@]username"
         :long-documentation
         "Uses markov chains to generate things that are likely for someone to say.

Accepts an optional 'sanity' argument which changes the k-order of the markov chain."))

  (setf (gethash "markovregexp" *responses*)
        (make-instance
         'flourgrinder-command
         :func (a:curry #'command-markov t nil nil)
         :documentation "[sanity: 0-10] 'regexp'"
         :long-documentation
         "Uses markov chains to generate things that are likely for someone to say.

Accepts an optional 'sanity' argument which changes the k-order of the markov chain.

This version takes in a regexp and will seed the markov with any user that matches the regexp."))

  (setf (gethash "markovlimit" *responses*)
        (make-instance
         'flourgrinder-command
         :func (a:curry #'command-markov nil t nil)
         :documentation " (limit: int) [sanity: 0-10] 'regexp'"
         :long-documentation
         "Uses markov chains to generate things that are likely for someone to say.

Accepts an optional 'sanity' argument which changes the k-order of the markov chain.

This version takes in a limit as well to parse only the last x messages by a user."))

  (setf (gethash "markovinline" *responses*)
        (make-instance
         'flourgrinder-command
         :func (a:curry #'command-markov nil nil t)
         :documentation " [sanity: 0-10] 'text'"
         :long-documentation
         "Uses markov chains to generate things that are likely for someone to say.

Accepts an optional 'sanity' argument which changes the k-order of the markov chain.

This version takes in inline text instead of things said by a user."))

  (setf (gethash "source" *responses*)
        (make-instance
         'flourgrinder-command
         :func #'command-source
         :documentation " (regexp)"
         :long-documentation
         "Try to match the first time someone said a regexp"))

  (setf (gethash "fortune-mod" *responses*)
        (make-instance
         'flourgrinder-command
         :func
         (lambda (_arguments)
           (declare (ignore _arguments))
           (handler-case (send-message
                          (uiop:run-program "fortune -o"
                                            :output :string))
             (error (c)
               (send-message (format nil "> ~A~%" c)))))
         :documentation ""))

  (setf (gethash "roll" *responses*)
        (make-instance
         'flourgrinder-command
         :func
         (lambda (args)
           (let* ((parts (str:split "d" (first args)))
                  (parts (if (str:emptyp (first parts)) (rest parts) parts))
                  (first-num
                   (and parts
                        (parse-integer (first parts) :junk-allowed t)))
                  (second-num
                   (and parts
                        (parse-integer (str:join "" (rest parts)) :junk-allowed t)))
                  (first-num (when (and first-num (> 9999 first-num 0))
                               first-num))
                  (num nil)
                  (num-trail nil))
             (setf num
                   (cond
                     ((and first-num second-num)
                      ;; We got two numbers, roll the dice that many times.
                      (dotimes (x first-num)
                        (push (1+ (random second-num)) num-trail))
                      (reduce #'+ num-trail))
                     (first-num
                      (1+ (random first-num)))))
             (cond
               ((and num (>= 20 (length num-trail)) first-num second-num (>= 500 second-num))
                (send-message
                 (format nil "Rolled [~{~A~^ + ~}]: ~A" num-trail num)))
               (num
                (send-message
                 (format nil "Rolled a ~A!~%" num)))
               (t
                (send-message
                 (str:concat
                  (str:join
                   " "
                   (list
                    (a:random-elt '("Georgie" "Lawrence" "Venbellis" "Ilesys" "Mace" "Yelran" "Droop"))
                    (a:random-elt '("instantly" "slowly" "cautiously" "aggressively" "drunkenly" "sleepily"))
                    (a:random-elt '("filets" "freezes" "melts" "destroys" "eviscerates" "sneezes on" "proposes to" "slits" "chops" "sings to"))
                    (a:random-elt '("themselves" "the orc" "the bugbear" "Larry" "the goblin" "the zombie" "the dragon" "the DM" "the ghost"))
                    "with their"
                    (a:random-elt '("trusty" "worn down" "burning" "frozen" "imaginary" "gigantic" "legendary" "garbage"))
                    (a:random-elt '("bow" "scimitar" "longsword" "warhammer" "battle axe" "holy scripture" "ornate staff" "goodberies"))))
                  "!"))))))
         :documentation "[die-size]"))

  (setf (gethash "shoot" *responses*)
        (make-instance
         'flourgrinder-command
         :func
         (lambda (arguments)
           (let* ((weapon-parts (a:random-elt *default-weapon-alist*))
                  (target (str:trim (str:join " " arguments))))
             (if (str:emptyp target)
                 (send-message (a:random-elt *failure-responses*))
                 (send-message
                  (if (cl-ppcre:scan "~A" (cdr weapon-parts))
                      (format nil "~A loads its ~A and ~A"
                              *BOT-NAME*
                              (car weapon-parts)
                              (format nil (cdr weapon-parts) target))
                      (format nil "~A loads its ~A and ~A ~A"
                              *BOT-NAME* (car weapon-parts)
                              (cdr weapon-parts) target))))))
         :documentation "(name)"))

  (setf (gethash "random" *responses*)
        (make-instance
         'flourgrinder-command
         :func
         (lambda (arguments)
           (let* ((user (squash-args arguments))
                  (reply (user-random-message user)))
             (if reply
                 (send-message reply)
                 (send-message (a:random-elt *failure-responses*)))))
         :documentation "[@]username"))

  (setf (gethash "echo" *responses*)
        (make-instance
         'flourgrinder-command
         :func
         (lambda (arguments)
           (a:if-let ((msg (squash-args arguments)))
               (if msg
                   (send-message msg)
                   (send-message (a:random-elt *failure-responses*)))))
         :documentation "Echo the given arguments"))

  (setf (gethash "m" *response-alias*) "markov")
  (setf (gethash "mr" *response-alias*) "markovregexp")
  (setf (gethash "ml" *response-alias*) "markovlimit")
  (setf (gethash "mi" *response-alias*) "markovinline")
  (setf (gethash "r" *response-alias*) "random")
  (setf (gethash "f" *response-alias*) "fortune-mod")
  (setf (gethash "s" *response-alias*) "shoot")
  ;; (setf (gethash "h" *response-alias*) "help")

  (setf (gethash "help" *responses*)
        (make-instance
         'flourgrinder-command
         :func
         (lambda (arguments)
           (declare (ignore arguments))
           (let* ((message (format nil "> Basic Commands:~%    ")))
             (maphash (lambda (k v)
                        (unless (typep v 'flourgrinder-command)
                          (setf message (format nil "~A ~A~A" message *wake-char* k))))
                      *responses*)
             (setf message (format nil "~A~%~%Normal Commands:~%    " message))
             (maphash (lambda (k v)
                        (when (typep v 'flourgrinder-command)
                          (setf message
                                (format nil "~A ~A~A ~A~%    "
                                        message *wake-char* k
                                        (command-docs v)))))
                      *responses*)
             (setf message (format nil "~A~%Aliases:~%    " message))
             (maphash (lambda (k v)
                        (setf message
                              (format nil "~A ~A -> ~A~%    "
                                      message k v)))
                      *response-alias*)
             (send-message message)))
         :documentation "")))

(defun handle-message (|name| |text| |user_id|)
  (cond
    ((eql (elt |text| 0) *wake-char*)
     (let* ((split-str (str:split-omit-nulls #\  |text|))
            (command (subseq (first split-str) 1))
            (arguments (rest split-str))
            (command-response (or (gethash command *responses*)
                                  (gethash
                                   (gethash command *response-alias*)
                                   *responses*))))
       (multiple-value-bind (not-throttled warning)
           ;; Don't check for a request if we don't have a command
           (if command-response
               (check-request |user_id|)
               t)
         (cond
           ((not not-throttled)
            (when warning
              (send-message (format nil "Initiating throttling for ~A!" |name|)))
            (format t "Throttling request from ~A~%" |name|))
           (command-response
            (cond ((stringp command-response)
                   (send-message command-response))
                  ((listp command-response)
                   (send-message
                    (trivial-string-template:safe-substitute
                     (a:random-elt command-response)
                     :bot *BOT-NAME* :user |name|)))
                  ((functionp command-response)
                   (funcall command-response arguments))
                  ((typep command-response 'flourgrinder-command)
                   (funcall (command-func command-response) arguments))
                  (t
                   (format t "Malformed hashtable on entry ~A~%" command))))
           (t
            (format t "No command matches ~A~%" command))))))
    (t
     ;; Bad hack to add good boys
     (when (cl-ppcre:scan "goo+d\\s*bo+[yi]+" (str:downcase |text|))
       (send-message (a:random-elt +GOODBOY-URLS+)))

     ;; No handler found, so we should log this as a legit message
     (user-add-hist |name| |text|))))

(define-easy-handler (my-greetings :uri "/bot") ()
  (setf (hunchentoot:content-type*) "text/text")
  (handler-bind ((error (lambda (e)
                          (send-message (format nil "> ~A~%" e)))))
    (let ((request-type (hunchentoot:request-method hunchentoot:*request*)))
      (when (eql request-type :post)
        (let* ((raw-data (hunchentoot:raw-post-data :force-text t))
               (json-object (jonathan:parse raw-data)))
          ;; (hunchentoot:log-message* :INFO "Received ~A from server" raw-data)
          (destructuring-bind
                (&key |name|
                      |text|
                      |user_id|
                      |sender_type| &allow-other-keys)
              json-object
            ;; Names can have multiple spaces in them. However, groupme strips
            ;; duplicate consecutive spaces. In order to allow de-referencing
            ;; them in groupme, strip consecutive white-space and replace with
            ;; a space.
            (setq |name| (str:trim (cl-ppcre:regex-replace-all "\\s+" |name| " ")))
            (if (and (> (length |text|) 0)
                     ;; No Bots!
                     (equalp |sender_type| "user"))
                (progn
                  (handle-message |name| |text| |user_id|)
                  "OK")
                (progn
                  ;; Log history for bots too
                  (user-add-hist |name| |text|)
                  ;; (hunchentoot:log-message* :INFO "Filtering out message: ~A" |text|)
                  "FAIL"))))))))

(defun discord-easy-handler (message)
  (when (and (not (string= "" (lc:content message)))
             (not (lc:botp (lc:author message)))
             (find (lc:channel-id message) *whitelist-channels*))

    (let* ((*discord-cur-message* message)
           (nick (or (a:when-let*
                         ((author (lc:author message))
                          (guild (lc:guild message))
                          (member (lc:member author guild))
                          (nick (lc:nick member)))
                       nick)
                     (lc:name (lc:author message)))))
      (handle-message nick
                      (lc:content message)
                      (lc:id (lc:author message))))))

(defun prompt-read (&optional (prompt "->"))
  (format *query-io* "~a " prompt)
  (finish-output *query-io*)
  (let ((value (read-line *query-io* nil nil)))
    (if value
        value
        (progn
          (format *query-io* "~%")
          "quit"))))

(defun main (&rest args)
  (declare (ignore args))

  ;; Define arguments
  (unix-opts:define-opts
    (:name :help
           :description "print this help text"
           :short #\h
           :long "help")
    (:name :bot-id
           :description "the groupme/discord bot_id to use"
           :short #\b
           :required t
           :arg-parser #'identity
           :long "bot-id")
    (:name :auth-token
           :description "The groupme auth token to use. Ignored on discord."
           :required t
           :arg-parser #'identity
           :long "auth-token")
    (:name :whitelist-channels
           :description "The channel ids to whitelist on discord."
           :required nil
           :arg-parser #'identity
           :long "whitelist-channels")
    (:name :server-port
           :description "the server port to use"
           :short #\p
           :long "port"
           :arg-parser #'parse-integer)
    (:name :frontend
           :description "The messaging frontend to use. Either groupme or discord."
           :short #\f
           :long "frontend"
           :arg-parser #'identity))

  (multiple-value-bind (options free-args)
      (unix-opts:get-opts)
    (declare (ignore free-args))
    (when (getf options :help)
      (opts:describe
       :prefix "A groupme bot to make it more enjoyable."
       :suffix "Let's hope this works"
       :usage-of "./flourgrinder")

      (setq *QUIT* t))
    (when (getf options :bot-id)
      (setf *bot-id* (getf options :bot-id)))
    (when (getf options :whitelist-channels)
      (setf *whitelist-channels*
            (mapcar (a:rcurry #'parse-integer :junk-allowed t)
                    (str:split "," (getf options :whitelist-channels)))))
    (when (getf options :auth-token)
      (setf *auth-token* (getf options :auth-token)))
    (when (getf options :port)
      (setf *server-port* (getf options :port)))
    (when (getf options :frontend)
      (setf *frontend* (getf options :frontend))))


  (unless *QUIT*
    (init-commands)

    (cond
      ((groupme-p)
       (format *query-io* "starting webserver on port ~A~%" *server-port*)
       (finish-output *query-io*)
       (hunchentoot:start *h*))
      ((discord-p)
       (lispcord:set-log-level :info)
       (lispcord:defbot *discord-bot* *bot-id*)
       (lispcord:connect *discord-bot*)
       (lispcord:add-event-handler
        :on-message-create
        #'discord-easy-handler)
       ;; I hope this is not needed, we'll see
       (lispcord:add-event-handler
        :on-close
        (lambda (&rest _)
          (declare (ignore _))
          (save-userhist)
          (cl-user::quit)
          ;; TODO fix this reconnection instead of just bailing
          ;; Currently when reconnecting a thread goes to 100% in futex, seems to be inside websocket-client-driver. Too lazy to fix.

          ;; (bordeaux-threads:make-thread
          ;;  (when (not *QUIT*)
          ;;    (lambda ()
          ;;      (format t "Disconnected, queuing reconn...~%")
          ;;      (sleep 10)
          ;;      (bordeaux-threads:with-lock-held (*discord-reconn-lock*)
          ;;        (format t "~A vs ~A~%" *DISCORD-RECONN-TIME* (get-universal-time))
          ;;        (when (and (not (lispcord.core:bot-session-id *discord-bot*))
          ;;                   (< (+ 10 *DISCORD-RECONN-TIME*) (get-universal-time)))
          ;;          (setf *DISCORD-RECONN-TIME* (get-universal-time))
          ;;          (format t "Attempting manual reconnection!!!~%")
          ;;          (lispcord:connect *discord-bot*))))))
          ))))
    (loop
       (let ((command (prompt-read)))
         (cond ((or (equal command "quit")
                    (equal command "q"))
                (setq *QUIT* t)
                (cond
                  ((groupme-p)
                   (hunchentoot:stop *h*))
                  ((discord-p)
                   (lispcord:disconnect *discord-bot* "shutdown" 123)))

                (save-userhist)
                (return command))
               ((equal command "save")
                (save-userhist)
                (format *query-io* "Saved!~%"))
               ((equal command "debug")
                (setf *NO-SEND* (not *NO-SEND*))
                (setf *throttle-enable* (not *throttle-enable*))
                (format *query-io* "Sending status: ~A. Throttle status: ~A~%"
                        (not *NO-SEND*)
                        *throttle-enable*))
               ((equal command "reload-code")
                (init-commands)
                (ql:quickload :flourgrinder)))))))
