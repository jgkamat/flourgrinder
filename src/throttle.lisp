(in-package :flourgrinder)

(defvar *throttle-enable* nil)
(defvar *time-block*
  ;; 1 hour
  (* 60 60))
(defvar *rate-limit* 5)

;; user-id -> (first-request-time number-requests warned)
(defvar *rate-ht* (make-hash-table :test #'equal))

(defvar *rate-whitelist* '(21173995))

(defun check-request (user-id)
  "See if we can accept another request.
Return nil if we cannot.
Second return value is if a warning should be sent.
When returning nil, tracking is not changed."
  (macrolet ((ht-slot ()
               '(gethash user-id *rate-ht*)))
    (if (and *throttle-enable*
             (not (find user-id *rate-whitelist*)))
        (cond
          ((and (ht-slot)
                (> (- (get-universal-time)
                      (first (ht-slot)))
                   *time-block*))
           ;; Time slot expired, reset request amount
           (setf (ht-slot) (list (get-universal-time) 1 nil)))
          ((and (ht-slot)
                (>= (second (ht-slot)) *rate-limit*))
           ;; Exceeded rate limit!
           (multiple-value-prog1
               (values nil (not (third (ht-slot))))
             (setf (third (ht-slot)) t)))
          ((ht-slot)
           (incf (second (ht-slot))))
          (t
           (setf (ht-slot) (list (get-universal-time) 1 nil))))
        t)))
