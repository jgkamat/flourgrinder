(in-package :flourgrinder)

(defparameter +GOODBOY-URLS+
  (or (let ((in (open "./seeds/goodboy" :if-does-not-exist nil)))
        (when in
	        (prog1
              (loop for line = (read-line in nil)
                 while line collect line)
            (close in))))
      '("No good boys found...")))
