
(in-package :flourgrinder)

(defparameter *default-weapon-alist* '(("ICBM silo" . "nukes")
                                       ("water gun" . "drenches")
                                       ("flame thrower" . "roasts")
                                       ("tranquilizer gun" . "knocks out")
                                       ("Visual Studio Enterprise Edition" . "refactors away")
                                       ("blunderbuss" . "hits everything but")
                                       ("monad" . "encapsulates")
                                       ("XML generator" . "makes tag soup out of")
                                       ("Mitsubishi A6M Zero" . "breaks divine wind on")
                                       ("postgresql prompt" . "shards")
                                       ("autotools" . "#ifdefs ~A out of existence")
                                       ("Windows XP botnet" . "DDoSes")
                                       ("taser" . "electrocutes")
                                       ("git" . "merge --squashes")
                                       ("WoW account" . "kills ~A's avatar")
                                       ("fflush" . "writes ~A to disk")
                                       ("sniper rifle" . "assassinates")
                                       ("Colt Peacemaker" . "pacifies")
                                       ("RFC1149-compliant avian carrier" . "UDP floods")
                                       ("legislative branch" . "leaves ~A behind")
                                       ("Batarang" . "knocks ~A down")
                                       ("λ calculus" . "β reduces")
                                       ("common lisp" . "macroexpands")
                                       ("shell" . "fork bombs")
                                       ("ghci" . "type checks")
                                       ("finite automaton" . "rejects")
                                       ("garbage collector" . "reclaims")
                                       ("lightning bolt" . "sends ~A to tartarus")
                                       ("lawyer" . "starts criminal proceedings against")
                                       ("shyster" . "SLAPPs ~A upside the haid")
                                       ("SOAP" . "describes ~A in WSDL")
                                       ("Aperture Science Emergency Intelligence Incinerator" . "euthanizes")
                                       ("cat launcher" . "unleashes a clawnado upon")
                                       ("bitkeeper" . "locks in")
                                       ("fish catapult" . "buries ~A in flounder")
                                       ("Facebook account" . "tags ~A in a scandalous photograph")
                                       ("nailgun" . "crucifies")
                                       ("Nickelback Greatest Hits" . "forces ~A to listen")
                                       ("axiom of choice" . "reassembles ~A into two identical copies")
                                       ("lawyer" . "litigates")
                                       ("ld50" . "ODs")
                                       ("WD40" . "lubricates")
                                       ("awk" . "shoves ~A through a one-liner")
                                       ("pixellated sword" . "whacks ~A repeatedly")
                                       ("Ballmertron" . "chairminates")
                                       ("silenced pistol" . "grabs and double taps")
                                       ("hammer" . "thors")
                                       ("mom" . "grounds")
                                       ("/usr/bin/git" . "detaches ~A's HEAD")
                                       ("Disney villain" . "executes a bizarre and error-prone plan against")
                                       ("broken bottle" . "mutilates")
                                       ("IRC bot" . "flames")
                                       ("[UNPRINTABLE]" . "[UNPRINTABLE]s")
                                       ("four horses" . "quarters")
                                       ("psychopath" . "brutally murders")
                                       ("sharks with frickin' laser beams attached to their heads" . "fails to kill ~A")
                                       ("troll" . "trolls")
                                       ("railgun" . "frags")
                                       ("snake launcher" . "boa constricts")
                                       ("e-meter" . "performs a walletectomy on")
                                       ("bzero" . "nullifies")
                                       ("ritual sacrifice kit" . "sacrifices ~A to some pagan god with a beard")
                                       ("rolled-up newspaper" . "whacks ~A on the nose")
                                       ("flame thrower" . "kills ~A with fire")
                                       ("unit-testing framework" . "tests ~A into submission")
                                       ("compiler" . "reduces ~A to byte code")
                                       ("lynch mob" . "lynches")
                                       ("droning professor" . "destroys ~A's will to live")
                                       ("anti-tank gun" . "obliterates")
                                       ("parser written in PHP" . "barfs all over")
                                       ("hipster" . "snubs")
                                       ("zombie horde" . "rips ~A limb from limb")
                                       ("hand of slapping" . "slaps")
                                       ("philosopher" . "gives ~A an existential crisis")
                                       ("captcha" . "questions ~A's humanity")
                                       ("crossbow" . "shoots an apple off ~A's head")
                                       ("sirens" . "lures ~A to their watery doom")
                                       ("lisp machines" . "makes ~A sound like a nostalgic kook")
                                       ("global interpreter lock" . "stalls ~A")
                                       ("gatling gun" . "guns down")
                                       ("gatling gnu" . "gnus down")
                                       ("GPL" . "infects")
                                       ("qi" . "hadoukens")
                                       ("fire flower" . "shoots fireballs at")
                                       ("banana peel" . "trips up")
                                       ("OED" . "pedantically defines the heck out of")
                                       ("object of some sort" . "uses it to attack ~A in a manner appropriate to that type of object")
                                       ("erbot" . ",shoots")
                                       ("IRC client" . "wastes the rest of ~A's day")
                                       ("Death Note" . "writes ~A's name in")
                                       ("plank" . "makes ~A walk off it")
                                       ("fanfic" . "burns ~A's eyes out")
                                       ("slashfic" . "pairs ~A with Jabba the Hutt")
                                       ("prude" . "censors")
                                       ("Elnode" . "serves")
                                       ("record player" . "funkifies")
                                       ("pedant" . "bores ~A to death")
                                       ("LA studio" . "showers ~A with cash and valuable prizes")
                                       ("neural lace" . "wires ~A up to the console as a shell")
                                       ("slamhound" . "hunts down and blows up")
                                       ("xterm" . "kill -9s")
                                       ("priest" . "condemns ~A to hell")
                                       (".44 magnum" . "blows ~A's head clean off")
                                       ("typing system" . "coerces")
                                       ("proxy" . "anonymously vandalises ~A's Wikipedia page")
                                       ("Fox executive" . "arrests ~A development")
                                       ("swarm of clueless noobs" . "plagues ~A with silly questions")
                                       ("IED packed with cutlery" . "fork bombs")
                                       ("github" . "fork bombs")
                                       ("voodoo doll" . "remotely tortures")
                                       ("witch doctor" . "shrinks ~A's head")
                                       ("railgun" . "pod-kills")
                                       ("legumbre" . "defadvices")
                                       ("salty ad hominem" . "seasons")
                                       ("ESR kernel patches" . "sends ~A -SIGSAUER")
                                       ("earworm" . "infiltrates ~A's auditory center")
                                       ("GNU HURD" . "is unable to run")
                                       ("GNU Image Manipulation Program" . "shops hookers and blow into a picture of")
                                       ("C string" . "null terminates")
                                       ("rump trumpet" . "plays a concerto for")
                                       ("obscure reference" . "smugly smiles at ~A's confusion")
                                       ("blackboard eraser" . "lobs it at")
                                       ("spanish inquisition" . "gives ~A 30 days notice for their heresy trial")
                                       ("trebuchet" . "flings a dead horse at")
                                       ("oil money" . "buries ~A with pseudoscience")
                                       ("json encoder" . "uses ~A for evil")
                                       ("Jack Nicholson" . "leers creepily at")
                                       ("./configure" . "checks ~A for Fortran 77")
                                       ("baseball bat" . "hits ~A out of the park")
                                       ("4chan" . "drops dox on")
                                       ("goldplated cable" . "plugs ~A into a tube amplifier")
                                       ("feces" . "flings it at")
                                       ("laundry line" . "hangs ~A out to dry")
                                       ("banana" . "bashes")
                                       ("lead pipe" . "frames ~A for killing Dr. Black in the Library")
                                       ("recycling plant" . "recycles")
                                       ("CRT monitor" . "slowly gives ~A radiation poisoning")
                                       ("warehouse full of IBM XTs equipped with 300 baud modems" . "MS-DDoSes")
                                       ("erlang" . "hotswaps in a newer, more concurrent, and more fault tolerant version of")
                                       ("Smalltalk image and changes files" . "serialises ~A into a pile of inseparable, partially binary objects")
                                       ("wordy, obscure, and surreal piece of prose" . "turns ~A into a non-pipe")
                                       ("気". "波動拳s")
                                       ("bottle of water" . "sprays")
                                       ("holy hand grenade of antioch" . "counts to five, no three, and then throws at")
                                       ("hungry hungry hippo" . "gulps down")
                                       ("identity function" . "returns ~A untouched")
                                       ("cookie jar" . "hands ~A the biggest one")
                                       ("denotational semantics" . "transforms the world")
                                       ("teacher qualifications" . "flunks")
                                       ("T_PAAMAYIM_NEKUDOTAYIM" . "resolves ~A's scope")
                                       ("sledgehammer" . "knocks down ~A's load-bearing limbs")
                                       ("oven" . "bakes a cake for")
                                       ("microscopic flesh-eating hamster" . "hides it somewhere in ~A's favourite sweater")
                                       ("experimental cocktail of drugs" . "injects into")
                                       ("whip" . "flogs")
                                       ("UML Diagramming Tool" . "specifies the heck out of ~A")
                                       ("DOSEMU botnet" . "FreeDDOSes")
                                       ("Roman Army officers and/or FFT implementation" . "decimates")
                                       ("gpg" . "elliptically encrypts ~A")
                                       ("holy water" . "exorcises the demons from")
                                       ("a shoe box" . "saves ~A for a rainy day")
                                       ("tattooist" . "tattoos VIM in a heart on ~A's arm")
                                       ("wooden shoe" . "throws it into ~A's gears")
                                       ("magician" . "saws ~A in half")
                                       ("scare quotes" . "“shoots”")
                                       ("social media 2.0 site" . "unfriends")
                                       ("cable TV" . "subjects ~A to hours of Disney sitcoms")
                                       ("R" . "reveals ~A to be statistically insignificant")
                                       ("oven" . "bakes ~A into a cake")
                                       ("stake" . "burns")
                                       ("steak" . "asks ~A how they would like it cooked")
                                       ("not very funny firearm" . "does something slightly silly, but ultimately unamusing to")
                                       ("scotsman" . "burns ~A's supper")
                                       ("pope" . "excommunicates")
                                       ("guillotine" . "beheads")
                                       ("k-pop" . "blasts")
                                       ("surround-sound system" . "shatters ~A's skull with soundwaves")
                                       ("sicario and/or git client" . "detaches ~A's HEAD")
                                       ("augean stables" . "hands ~A a mop and bucket")
                                       ("baboonzooka" . "shows ~A its buttocks while launching projectiles over its shoulder")
                                       ("slashdotfic" . "pairs ~A with CmdrTaco and plenty of hot grits")
                                       ("gfdl" . "makes all ~A worst features invariant")
                                       ("dual" . "coshoots co~A")
                                       ("royal canadian mountie" . "foils ~A again")
                                       ("“Li'l Bastard” Identity Theft Kit" . "masquerades as")
                                       ("stephen colbert" . "roasts")
                                       ("carpet" . "rolls ~A up in it and dumps them off the pier")
                                       ("giant mirror array" . "burns ~A from afar")
                                       ("cps-transform" . "delimits ~A's continuation")
                                       ("J.J. Abrams series" . "leaves ~A hanging indefinitely")
                                       ("subprime mortgage" . "clogs ~A's equity of redemption")
                                       ("questionable first date" . "makes ~A watch Police Academy 14")
                                       ("MS-DOS disk error" . "Aborts, Retries, and Fails")
                                       ("lennart poettering" . "reinvents ~A in a better/worse way")
                                       ("incriminating photographs" . "blackmails")
                                       ("Dirty Harry" . "with a .44 Magnum, the most powerful handgun in the world, blows the head clean off of")
                                       ("Neal Stephenson" . "drops a cinder block of a tome on")
                                       ("Evgeny Morozov" . "mocks ~A as a naive technological solutionist")
                                       ("phaser" . "stuns")
                                       ("formal grammar" . "frees ~A's context")
                                       ("TSA agent" . "subjects ~A to an excessively thorough body cavity search")
                                       ("closure" . "closes over ~A and whispers sweet nothings via lexical bindings")
                                       ("data flow analysis" . "eliminates ~A as useless")
                                       ("Jacquard loom" . "compiles ~A from punchcards into fabric")
                                       ("pack of women with fake beards" . "stones")
                                       ("1/8th of weed" . "stones")
                                       ("12 pack of andrex double quilted" . "TPs ~A's house")
                                       ("poison" . "poisons")
                                       ("poisson" . "determines the likelihood of ~A being killed by a horse kick")
                                       ("maintainer" . "closes ~A's bug report as WONTFIX")
                                       ("window" . "defenestrates")
                                       ("toll-free support line" . "puts ~A on hold")
                                       ("school teacher" . "flunks")
                                       ("vorpal blade" . "snicker snacks")
                                       ("La Marzocco" . "pulls a shot at")
                                       ("potato knife" . "peels")
                                       ("grater" . "zests")
                                       ("tvtropes" . "steals an hour of ~A's life")
                                       ("bouquet" . "showers ~A with rose petals")
                                       ("bag of porridge" . "suffocates")
                                       ("tali713" . "pcases")
                                       ("chainsaw" . "performs a root canal on")
                                       ("youtube url" . "rickrolls")
                                       ("/ignore" . "plonks")
                                       ("Hotel California" . "makes a reservation for")
                                       ("schoolmaster" . "canes")
                                       ("black knight" . "impales")
                                       ("deep fryer" . "dunks ~A until they're golden brown")
                                       ("lich mob" . "enthralls")
                                       ("mail loop" . "DoSes ~A's mail spool")
                                       ("bureaucrat" . "makes ~A fill out paperwork to register their autoloads")
                                       ("socat" . "transparently proxies")
                                       ("peat bog" . "mummifies")
                                       ("MacBook" . "promiscuously announces its presence to ~A over mdns")
                                       ("firing squid" . "inks")
                                       ("privy council" . "bestows upon ~A a letter-patent")
                                       ("UUCP mailserver" . "sends ~A a flame from jordanb")
                                       ("Myers-Briggs Type Indicator" . "reduces ~A to a four-letter type")
                                       ("special forces team" . "plays Celine Dion albums to break ~A's will to resist")
                                       ("ICBF silo" . "doesn't give a fuck about")
                                       ("secretary bird" . "stomps and kicks ~A to a pulp")
                                       ("virtual mirror neuron" . "determines that ~A is not a cat")
                                       ("roguelike" . "gives ~A a useful but inconvenient vestigial tail")
                                       ("fizzbuzz" . "disqualifies as a programmer")
                                       ("24-hour news channel" . "demonizes ~A as an Internet hate machine")
                                       ("fixed-point iterator" . "minimizes")
                                       ("Chekhov's gun" . "shoots ~A in the third act")
                                       ("entirety of MELPA" . "makes the init.el of ~A take 5 years to evaluate")
                                       ("NFS" . "tramples all over ~A's files, ignoring locks")
                                       ("shiv" . "stabs ~A in the gut and then runs off into the shadows")
                                       ("unimaginative weapon" . "attacks ~A in an unimaginative way")
                                       ("symmetric group" . "permutates ~A's organs")
                                       ("haberdasher" . "bowlerizes")
                                       ("omnipotent powers of attribution" . "blames ~A for 9/11")
                                       ("Ruth Reichl" . "gives a scathing review to")
                                       ("Emacs/vi slash fiction" . "reads to")
                                       ("porridge cannon" . "slimes ~A until they're stuck to the wall")
                                       ("isometric sword-and-sorcery RPG" . "slashes ~A with a sword until flying numbers fly out")
                                       ("minecraft" . "painstakingly assembles a statue of ~A in the town square")
                                       ("Bureau of Sabotage" . "saves ~A from their own excessive efficiency")
                                       ("VVP" . "registers ~A as a foreign agent")
                                       ("censor and/or spoiler alert" . "clads ~A in black bars")
                                       ("antique X client" . "sends ~A a SECONDARY selection atom of type APPLE_PICT")
                                       ("crazy old lady in rural America" . "gives ~A poisoned tea")
                                       ("old lace" . "gives ~A wine laced with arsenic")
                                       ("schnoz" . "sneezes on")
                                       ("rake" . "#@*$s ~A with it")
                                       ("shotgun" . "blasts") '("Windows 3.1 botnet" . "MS-DDoSes")
                                       ("docker" . "containerizes and deploys")
                                       ("running shoe" . "fires a Nike Missile at")
                                       ("anachronism" . "fires a Tomahawk Missile at")
                                       ("dad joke" . "humors ~A patronizingly")
                                       ("PLM" . "end-of-lifes")
                                       ("FORTH" . "pushes ~A onto the stack beneath a Towers Of Hanoi solver")
                                       ("WOW" . "makes ~A fall from their chair")))
