(in-package :flourgrinder)

(ql:quickload :cl-csv)
(ql:quickload :cl-store)

;; Bulk import operations not meant to happen regularly

(defparameter *csv-file* #P"~/Downloads/flouricao.csv")
(defparameter csv-dump nil)
(defparameter user-hash (make-hash-table :test #'equal))

(defun process-csv ()
  "one-time converstion from csv to hash table"
  (setf csv-dump (cl-csv:read-csv *csv-file*))
  (format t "Done with csv ~%")
  (dolist (line (rest csv-dump))
    (let ((text (elt line 3))
          (author (elt line 2)))
      (setf (gethash author user-hash)
            ;; Append the new entry to the front
            (append (list text)
                    (gethash author user-hash)))))

  ;; Output to file
  (cl-store:store user-hash "./userhist"))
