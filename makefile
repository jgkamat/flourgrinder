

# Simple makefile loading the main file, using sbcl. Similar steps can be taken for other lisps
FLAGS=--output run/flourgrinder --asdf-path . --load-system flourgrinder --entry flourgrinder:main
FLOURGRINDER_FLAGS=
SBCL_FLAGS=--noinform \
					 --eval "(push (truename \".\") asdf:*central-registry*)" \
					 --eval "(ql:quickload :flourgrinder)" \
					 --disable-debugger
SBCL_TAIL_FLAGS=--end-toplevel-options $(FLOURGRINDER_FLAGS)
NONINTERACTIVE=--non-interactive

RUN_FLAG=--eval "(flourgrinder:main)"
IN_PKG=--eval "(in-package :flourgrinder)"
QUIT_FLAG=--eval "(quit)"

all:
	sbcl $(SBCL_FLAGS) $(NONINTERACTIVE) $(QUIT_FLAG) $(SBCL_TAIL_FLAGS)

dir:
	mkdir -p run

run:
	sbcl $(SBCL_FLAGS) $(RUN_FLAG) $(SBCL_TAIL_FLAGS)
r: run

debug:
	sbcl $(SBCL_FLAGS) --eval "(declaim (optimize (debug 3)))" $(IN_PKG) $(SBCL_TAIL_FLAGS)

release:dir
	buildapp --load ~/.sbclrc --compress-core $(FLAGS)


clean:
	-rm -rf ./run
	-find . -name '*.fasl' | xargs rm -f

alt:
	sbcl --load build.lisp --name flourgrinder

win:
	wine sbcl --load build.lisp --name flourgrinder


.PHONY: run
